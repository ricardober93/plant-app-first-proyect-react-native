import React, { useEffect, useState } from "react";
import { StyleSheet, View, ActivityIndicator } from "react-native";
import Navigation from "./app/navigation";


function App() {
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading(!loading)
    }, 1000);
    return () => {
      clearTimeout(timer)
    }
  }, [])

  return (
    <View style={style.container}>
      {
        !loading ? <ActivityIndicator size="large" color="#00ff00" /> : <Navigation />
      }
    </View>
  );

}
const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  }
})

export default App;