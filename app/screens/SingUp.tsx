import { ParamListBase } from '@react-navigation/native'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import React from 'react'
import { Alert, Button, SafeAreaView, StyleSheet, Text, TextInput, View } from 'react-native'

interface Props {
  navigation: NativeStackNavigationProp<ParamListBase>;
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  title: {
    fontWeight: '700',
    fontSize: 28,
    textAlign: 'center',
    color: 'tomato',
    marginBottom: 30
  },
  label: {
    fontSize: 16,
    fontWeight: '500',
    marginHorizontal: 10,
  },
  form: {
   
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  area: {
    width: '100%',
    padding: 18,
  },
  input: {
    width: '100%',
    height: 40,
    marginVertical: 12,
    borderWidth: 1,
    padding: 10,
  },
  buttonLogin: {
    marginTop: 10
  }
})


const SingUp = (props: Props) => {
  const [text, onChangeText] = React.useState("");
  const [password, onChangePassword] = React.useState("");


  const GotoExplore = () => {
    if (text === 'user' && password === 'user') {
      props.navigation.navigate('Explore')
    } else {
      Alert.alert('Usuario: user. Password: user')
    }
  }

  return (
    <View style={style.container}>
      <SafeAreaView style={style.area} >
        <Text style={style.title}> Inicia sesión</Text>
        <View style={style.form}>
          <Text style={style.label} > Usuario</Text>
          <TextInput
            style={style.input}
            onChangeText={onChangeText}
            value={text}/>
          <Text style={style.label}> Password</Text>
          <TextInput
            textContentType="password"
            secureTextEntry={true}
            showSoftInputOnFocus={true}
            style={style.input}
            value={password}
            onChangeText={onChangePassword} />
        </View>
        <View>
          <Button
            title={'Login'}
            onPress={GotoExplore}
            color="tomato"
          />
        </View>
      </SafeAreaView>
    </View>
  )
}

export default SingUp

const styles = StyleSheet.create({})
