
import { ParamListBase } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack/lib/typescript/src/types';
import React from 'react'
import { Alert, StyleSheet, Text, View, SafeAreaView } from 'react-native'
import ButtonContainer from '../components/Button'


interface Props {
  navigation: NativeStackNavigationProp<ParamListBase>;
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  wellcome: {
    fontWeight: '700',
    fontSize: 24,
    textAlign: 'center',
    color: '#1a2b3c'
  },
  title: {
    fontWeight: '700',
    fontSize: 28,
    textAlign: 'center',
    color: 'tomato',
    marginBottom:30
  },    
  area: {
    width: '100%',
    padding:18,
  },
  buttonIniciar: {
    marginTop:10
  },
  buttonRegistrarse: {
    marginTop: 10
  }
})

function Wellcome({ navigation }: Props) {
  return (
    <View style={style.container}>
      <SafeAreaView style={style.area}>
        <Text style={style.wellcome}>Wellcome</Text>
        <Text style={style.title}>PLatnApp</Text>
        <View style={style.buttonIniciar}>
          <ButtonContainer
            text="Iniciar Sesión"
            onClick={() => navigation.navigate('SingUp')} />
        </View>
        <View style={style.buttonRegistrarse}>
          <ButtonContainer
            text="Registrarse"
            onClick={() => Alert.alert("Registrarse")} />
        </View>
     </SafeAreaView>
    </View>
  )
}

export default Wellcome
