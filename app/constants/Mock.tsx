const categories = [
  {
    id: 'plants',
    name: "Plants",
    tags: ['products', 'inspirations',],
    count: 147,
    image: require('../assets/images/leaf.png')
  },
  {
    id: 'Seeds',
    name: "seeds",
    tags: ['products', 'shop'],
    count: 16,
    image: require('../assets/images/seed.png')
  },
  {
    id:'flowers',
    name: "Flowers",
    tags: ['products', 'inspirations'],
    count: 56,
    image: require('../assets/images/flowers.png')
  },
  {
    id: 'sprayers',
    name: "Sprayers",
    tags: ['inspirations', 'shop'],
    count: 24,
    image: require('../assets/images/sprayer.png')
  },
  {
    id: 'pots',
    name: "Pots",
    tags: ['products', 'inspirations'],
    count: 79,
    image: require('../assets/images/plant-pot.png')
  },
  {
    id: 'fertilizers',
    name: "Fertilizers",
    tags: ['products', 'shop'],
    count: 147,
    image: require('../assets/images/fertilizante.png')
  },
]

const products = [
  {
    id: 1,
    name: "16 best plants for your living room",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed ullamcorper morbi tincidunt ornare massa eget egestas purus. Nisl vel pretium lectus quam. Tellus id interdum velit laoreet id donec. Facilisis volutpat est velit egestas. Ipsum a arcu cursus vitae congue mauris rhoncus. Vel pharetra vel turpis nunc eget lorem. Consequat interdum varius sit amet. Quam elementum pulvinar etiam non.",
    tags: ['interior', '27 m2', 'ideas'],
    gallery: [
      require('../assets/images/plants_1.jpg'),
      require('../assets/images/plants_2.jpg'),
      require('../assets/images/plants_3.jpg'),

      require('../assets/images/plants_4.jpg'),
      require('../assets/images/plants_5.jpg'),
      require('../assets/images/plants_6.jpg'),
    ]
  },
]

const explore = [
  //images
  require('../assets/images/explore_1.jpg'),
  require('../assets/images/explore_2.jpg'),
  require('../assets/images/explore_3.jpg'),
  require('../assets/images/explore_4.jpg'),
  require('../assets/images/explore_5.jpg'),
  require('../assets/images/explore_6.jpg'),
]

const profile = {
  username: "react-ui-kit",
  location: "Colombia",
  email: "ricardoTellez7@hotmail.com",
  avatar: require('../assets/images/explore_6.jpg'),
  budget: 2500,
  monthly_cap: 5000,
  notificacions: true,
  newsletter:false
}

export {
  categories,
  products,
  profile,
  explore
}