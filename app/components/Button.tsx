import React from 'react'
import { Button  } from 'react-native'

type Props = {
  text: string,
  onClick: any
}

export default function ButtonContainer({ text, onClick }: Props) {
  return (
    <Button
      title={text}
      onPress={onClick}
      color="tomato"
    />
  )


}
