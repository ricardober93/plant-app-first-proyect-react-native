import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import SingUp from "../screens/SingUp";
import Wellcome from "../screens/Wellcome";
import Login from "../screens/Login";
import Explore from "../screens/Explore";
import Product from "../screens/Product";
import Setting from "../screens/Setting";
import Browse from "../screens/Browse";




const Stack = createNativeStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerTintColor: 'white',
          headerStyle: { backgroundColor: 'tomato' },
        }}
        initialRouteName="Wellcome">
        <Stack.Screen options={{
          title: 'Iniciar Sesión',
        }}
          name="SingUp" component={SingUp} />
        <Stack.Screen name="Wellcome" component={Wellcome} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen options={{
          title: 'Explorar',
        }}
          name="Explore"
          component={Explore} />
        <Stack.Screen name="Setting" component={Setting} />
        <Stack.Screen name="Product" component={Product} />
        <Stack.Screen name="Browse" component={Browse} />
      </Stack.Navigator>
    </NavigationContainer>
  );

}

export default Navigation;